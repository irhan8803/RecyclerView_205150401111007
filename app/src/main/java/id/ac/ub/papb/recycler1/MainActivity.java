package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv1;
    Button submit;
    TextView nim, nama;
    MahasiswaAdapter adapter;
    public static String TAG = "RV1";
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        ArrayList<Mahasiswa> data = getData();
        adapter = new MahasiswaAdapter(this, data, new MahasiswaAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Mahasiswa mhs) {
                Bundle bundleMhs = new Bundle();
                bundleMhs.putString("nim", mhs.nim);
                bundleMhs.putString("nama", mhs.nama);
                intent.putExtras(bundleMhs);
                startActivity(intent);
            }
        });
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        submit = findViewById(R.id.bt1);
        submit.setOnClickListener(this);
        nim = findViewById(R.id.etNim);
        nama = findViewById(R.id.editTextTextPersonName2);
        intent = new Intent(this, Activity2.class);
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.bt1){
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.getText().toString();
            mhs.nama = nama.getText().toString();
            adapter.data.add(mhs);
            rv1.setAdapter(adapter);
            nim.setText("NIM");
            nama.setText("Name");
        }
    }
}