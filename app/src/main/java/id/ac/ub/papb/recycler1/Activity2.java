package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Bundle mhs = getIntent().getExtras();
        TextView nim = findViewById(R.id.tvNim2);
        TextView nama = findViewById(R.id.tvNama2);
        nim.setText("NIM : " + mhs.getString("nim"));
        nama.setText("Nama : " + mhs.getString("nama"));
    }
}